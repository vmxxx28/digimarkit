The versions I worked with:
PHP v7.3.21
MySQL v5.7.31 (for some reason on 000webhostapp.com this changes to MariaDB)
WAMP v3.2.3_x64

This project was created as way to certify my developer (programmer) skills for an internship position.

To test the project:
1. Go to https://juniortest-verners-mednis.000webhostapp.com/

OR

1. Download and install, and launch WAMP server!
   
2. Open your browser and go to "localhost/phpmyadmin"!
3. Create an empty new database and name it "digimarkit-db"!
4. Create a new table according to the settings in the picture "DB_Example.png", which can be found in this project folder.
   
5. Go to "localhost/add_vhost.php"
6. Set the name of the virtual host to "www.something.com"
7. Go to "C:/Windows/System32/driver/etc/hosts" and add these 2 lines at the end of the file:
   127.0.0.1	www.something.com
   ::1	www.something.com
   
8. Go to "project_digimarkit/app/core/Controller.php"
9. change 'Location: https://juniortest-verners-mednis.000webhostapp.com/' to 'Location https://www.something.com/'

10. Restart WAMP and now you can test the web app!
----------------------------------------------------------------------------------------------------------------------
Important notes!!!!!

If you can't launch the project, then please contact me via vmxxxxx@inbox.lv.
(or if on juniortest-verners-mednis.000webhostapp.com you see a database connection error)

If you want to launch this on WAMP server (or somewhere else other than juniortest-verners-mednis.000webhostapp.com),
then you must add a virtual host and name it www.something.com, otherwise the website is buggy and unsuable.
