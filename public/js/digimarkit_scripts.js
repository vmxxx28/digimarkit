//This a variable which stores the ID of the type-switcher. We have to remember its value in every function
//Initially it set to 0, because otherwise the script doesn't work (for some reason).
let type_specific_attributes = 0;

function switch_type()
{
    //In case this function gets called multiple times, we (re)hide the product-specific input fields.
    type_specific_attributes.hidden = true;

    //Now that the user has selected a (new) value, we show the (new) product-specific input fields.
    type_specific_attributes = document.getElementById(document.getElementById('type').value);
    type_specific_attributes.hidden = false;
}

function validate(fields_to_validate)
{
    //In case this function gets called multiple times, we (re)set the error messages to "".
    for(let i=0; i < fields_to_validate.length; i++) { fields_to_validate[i].nextSibling.innerHTML = ""; }

    let is_valid = true;

    //We check, if SKU is valid (For example: JWC000123)
    if (!fields_to_validate[0].value.match(/^([A-Za-z]{2,4}[0-9]{5,6})$/i)) {
        fields_to_validate[0].nextSibling.innerHTML = 'Please, provide the data of indicated type! (For example: JWC000123)'; is_valid = false;
    }
    //We check, if the user has typed in only letters and spaces
    if (!fields_to_validate[1].value.match(/^([ A-Za-z]*)$/i)) {
        fields_to_validate[1].nextSibling.innerHTML = 'Please, provide the data of indicated type! (Only letters and spaces are allowed)'; is_valid = false;
    }
    //We check, if the user has entered a valid $ value
    if (!fields_to_validate[2].value.match(/^([0-9][0-9]*(\.|,)[0-9][0-9])$/i)) {
        fields_to_validate[2].nextSibling.innerHTML = 'Please, provide the data of indicated type! (Only 2 numbers behind a decimal point)'; is_valid = false;
    }

    //ignore fields_to_validate[3], as there is nothing to validate

    //For the rest of the attributes we check, if the user has enetered an integer or a decimal
    for(let i=4; i < fields_to_validate.length; i++) {
        if (!fields_to_validate[i].value.match(/^([0-9][0-9]*((\.|,)[0-9][0-9]*)?)$/i)) {
            fields_to_validate[i].nextSibling.innerHTML = 'Please, provide the data of indicated type! (Only an integer or a decimal)'; is_valid = false;
        }
    }

    //We check if any of the fields are empty
    for(let i=0; i < fields_to_validate.length; i++) {
        if (fields_to_validate[i].value === "") {fields_to_validate[i].nextSibling.innerHTML = 'Please, submit required data!'; is_valid = false;}
    }

    //If at least one of the validations failed, then this function returns false
    return is_valid;
}

//When the user enters product-specific attributes, they are converted in the format NUMxNUMxNUM or NUM MB, or numKG.
function set_the_product_specific_attribute()
{
    let values = [];

    //We put product specific attribute values in an array,
    //so we can apply the values.join('x') function to concatenate them
    let attributes = Array.from(document.getElementsByClassName(type_specific_attributes.id+'-attribute'));
    for(let i=0; i < attributes.length; i++) { values[i] = attributes[i].value; }

    //type_specific_attributes.id contains "KG" or " MB", or " ".
    //We concatenate every number like this: NUM + x + NUM + x + NUM + id;
    document.getElementById('product-specific-attribute').value = values.join('x') + type_specific_attributes.id;

    //We must validate every input field before submission
    return validate([document.getElementById('SKU'), document.getElementById('name'), document.getElementById('price'), document.getElementById('type')].concat(attributes));
}

//This function can only be called when the window is in portrait mode.
//Once the user clicks on the hamburger button it shows/hides the nav bar.
function show_or_hide()
{
    let nav_bar = document.getElementById("nav-bar").style;

    //The fullHeight variable is needed to get the full height of the whole document.
    //It is then applied to the nav_bar's height attribute.
    let fullHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
    );

    //If nav_bar shown, then hide the nav_bar
    if (nav_bar.left === "0px")
    {
        nav_bar.animation="slide-back .5s";
        nav_bar.left = "-350px";
    }
    else //if nav_bar hidden then show the nav_bar
    {
        nav_bar.animation="slide .5s";
        nav_bar.left = "0px";
        nav_bar.height = fullHeight.toString() + "px";
    }
}