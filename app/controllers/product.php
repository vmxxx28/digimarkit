<?php

use Illuminate\Database\Capsule\Manager as DB;

class Product extends Controller
{
    public function __construct()
    {
    }

    public function list()
    {
        $data = DB::select('SELECT * FROM products');
        $this->view('product/list', $data);
    }

    public function add()
    {
        $this->view('product/add', []);
    }

    public function store()
    {
        if(isset($_POST['SKU']) and isset($_POST['name']) and isset($_POST['price']) and isset($_POST['type']) and isset($_POST['product_specific_attribute'])) {
            //In case, the user tyring to use SQL injections,
            //We execute the SQL statement only, if the data is valid
            if(preg_match("/^([A-Za-z]{2,4}[0-9]{5,6})$/", $_POST['SKU'])
            and preg_match("/^([ A-Za-z][ A-Za-z]*)$/", $_POST['name'])
            and preg_match("/^([0-9][0-9]*(\.|,)[0-9][0-9])$/", $_POST['price'])
            and isset($_POST['type'])
            and preg_match("/^([0-9][0-9]*((\.|,)[0-9][0-9]*)?x)*([0-9][0-9]*((\.|,)[0-9][0-9]*)?)[ A-Za-z]*$/", $_POST['product_specific_attribute'])) {
                DB::insert('INSERT INTO products (SKU, name, price, product_specific_attribute) VALUES (?, ? ,? ,?)',
                    [$_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['product_specific_attribute']]);
            }
        }
        $this->redirect('product/add', []);
    }

    public function delete()
    {
        $array_keys = implode(', ', array_keys($_POST));
        //In case, the user tyring to use SQL injections,
        //We execute the SQL statement only, if the data is valid
        if(preg_match("/^(([0-9]*, )*[0-9]*)$/", $array_keys)) {
            DB::delete('DELETE FROM products WHERE ID IN ('.$array_keys.')');
        }
        $this->redirect('product/list', []);
    }
}