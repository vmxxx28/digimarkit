<?php

use Illuminate\Database\Capsule\Manager as DB;

class Test extends Controller
{
    public function __construct()
    {
    }

    public function versions()
    {
        $results = DB::select( DB::raw("select version()") );
        $data['version'] = $results[0]->{'version()'};
        $this->view('test/versions', $data);
    }
}