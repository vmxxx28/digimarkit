<?php

class Controller
{
    public function model($model)
    {
       require_once '../app/models/'.$model.'.php';
       return new $model();
    }

    public function view($view, $data = [])
    {
        require_once '../app/views/'.$view.'.php';
    }

    public function redirect($url, $data = [], $statuscode = 303)
    {
        header('Location: https://juniortest-verners-mednis.000webhostapp.com/' . $url . '?' . http_build_query($data), true, $statuscode);
        die();
    }
}