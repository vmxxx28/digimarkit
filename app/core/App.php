<?php

class App
{
    protected $controller = 'home';

    protected $method = 'index';

    protected $params = [];

    public function __construct($data = [])
    {
        //Exapmle: url is "www.digimarkit.com/product/list/1/2"

        //set $url = [product, list, 1, 2]
        $url = $this->parseUrl();

        //set $this->controller = product
        if(isset($url[0])) {
            if(file_exists('../app/controllers/'.$url[0].'.php')) {
                $this->controller = $url[0];
                unset($url[0]);
            }
        }

        require_once '../app/controllers/'.$this->controller.'.php';

        $this->controller = new $this->controller;

        //set $this->method = list
        if(isset($url[1])) {
            if(method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        //if $url has any content then  set $this->params = [1, 2]
        //else set $this->params = [ ]
        $this->params = $url ? array_values($url) : [];

        //This function uses the "product" controller
        //and it passes "1" and "2" to the "list" method.
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseUrl()
    {
        if(isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}