<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DigiMarkit</title>
    <link href="../css/digimarkit_stylesheet.css" rel="stylesheet" type="text/css">
    <script src="../js/digimarkit_scripts.js"></script>
</head>
<body>
<!--- The form, which allows the user to add products to the database! --->
<!--- It is so large, because the input fields are in the main-div, but the submit (save) button is in the nav-bar. --->
<form id="add-product-form" method="post" onsubmit="return set_the_product_specific_attribute()">
    <!--- The nav bar --->
    <!--- (The hamburger button can't be seen, because the window needs to be in portrait mode to see it) --->
    <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
    <div id="nav-bar">
        <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
        <p class="big">Product Add</p>
        <ul>
            <li><input type="submit" value="SAVE" formaction="/product/store"></li>
            <li><input type="button" value="CANCEL" onclick="window.history.back();"></li>
            <li><input type="button" value="HOME PAGE" onclick="window.location.href='/';"></li>
        </ul>
    </div>
    <hr>
    <!--- The main div. It is different in every page --->
    <div id="main-div">
        <div>
            <p class="red"><?php if(isset($data['message'])){ $data['message']; } ?></p>
            <label for="SKU">SKU: </label> <input type="text" name="SKU" id="SKU"><p class="error-message"></p> <br>
            <label for="name">Name: </label> <input type="text" name="name" id="name"><p class="error-message"></p> <br>
            <label for="price">Price (in $): </label> <input type="text" name="price" id="price"><p class="error-message"></p> <br>
            <br>
            <br>
            <br>
            <label for="type">Type Switcher: </label> <select name="type" id="type" oninput="switch_type()">
                                                          <option value="" disabled selected hidden>Type Switcher</option>
                                                          <option value=" MB">DVD-Disc</option>
                                                          <option value="KG">Book</option>
                                                          <option value=" ">Furniture</option>
                                                      </select><p class="error-message"></p> <br>
            <div id="product-specific-attributes">
                <div id=" MB" hidden>
                    <label for="size">Size (MB): </label> <input type="text" name="size" class=" MB-attribute"><p class="error-message"></p> <br>
                    <p>Please, provide size!</p>
                </div>
                <div id="KG" hidden>
                    <label for="weight">Weight (KG): </label> <input type="text" name="weight" class="KG-attribute"><p class="error-message"></p> <br>
                    <p>Please, provide weight!</p>
                </div>
                <div id=" " hidden>
                    <label for="height">Height (CM): </label> <input type="text" id="height" class=" -attribute"><p class="error-message"></p> <br>
                    <label for="width">Width (CM): </label> <input type="text" id="width" class=" -attribute"><p class="error-message"></p> <br>
                    <label for="length">Length (CM): </label> <input type="text" id="length" class=" -attribute"><p class="error-message"></p> <br>
                    <p>Please, provide dimensions in HxWxL format!</p>
                </div>
            </div>
            <input name="product_specific_attribute" id="product-specific-attribute" type="hidden" value="">
        </div>
        <!--- I decided to add this image, since the page looked way too empty --->
        <div id="add-product-background-image-div">
            <img src="../images/add-product.png" alt="add-product background image missing">
        </div>
    </div>
</form>
<hr>
<!--- The info section --->
<div id="info-section">
    Scandiweb test assignment<br>
    Completed by Verners Mednis (vmxxxxx@inbox.lv)
</div>
</body>
</html>
