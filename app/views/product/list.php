<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DigiMarkit</title>
    <link href="../css/digimarkit_stylesheet.css" rel="stylesheet" type="text/css" >
    <script src="../js/digimarkit_scripts.js"></script>
</head>
<body>
<!--- The form, which allows the user to delete products from the database! --->
<!--- It is so large, because the input fields are in the main-div, but the submit (delete) button is in the nav-bar. --->
<form id="mass-delete-form" method="post">
    <!--- The nav bar --->
    <!--- (The hamburger button can't be seen, because the window needs to be in portrait mode to see it) --->
    <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
    <div id="nav-bar">
        <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
        <p class="big">Product List</p>
        <ul>
            <li><input type="button" value="ADD" onclick="window.location.href='/product/add';"></li>
            <li><input type="submit" value="MASS DELETE" formaction="/product/delete"></li>
            <li><input type="button" value="HOME PAGE" onclick="window.location.href='/';"></li>
        </ul>
    </div>
    <hr>
    <!--- The main div. It is different in every page --->
    <div id="main-div" class="grid-container">
        <?php
        foreach ($data as $data_item)
        {
            echo ('
            <div>
                <input type="checkbox" name="'.$data_item->ID.'"><br>
                '.$data_item->SKU.'<br>
                '.$data_item->name.'<br>
                '.$data_item->price.' $<br>
                '.$data_item->product_specific_attribute.'
            </div>
            ');
        }
        ?>
    </div>
</form>
    <hr>
    <!--- The info section --->
    <div id="info-section">
        Scandiweb test assignment<br>
        Completed by Verners Mednis (vmxxxxx@inbox.lv)
    </div>
</body>
</html>
