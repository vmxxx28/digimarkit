<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DigiMarkit</title>
    <link href="../css/digimarkit_stylesheet.css" rel="stylesheet" type="text/css">
    <script src="../js/digimarkit_scripts.js"></script>
</head>
<body>
    <!--- The nav bar --->
    <!--- (The hamburger button can't be seen, because the window needs to be in portrait mode to see it) --->
    <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
    <div id="nav-bar">
        <img class="hamburger-button" src="../images/hamburger-button.png" onclick="show_or_hide()" alt="hamburger-button image missing">
        <p class="big">Home Page</p>
        <ul>
            <li><input type="button" value="PRODUCT LIST PAGE" onclick="window.location.href='/product/list';"></li>
            <li><input type="button" value="PRODUCT ADD PAGE" onclick="window.location.href='/product/add';"></li>
        </ul>
    </div>
    <hr>
    <!--- The main div. It is different in every page --->
    <!--- This is the only page with no info --->
    <div id="main-div">
    </div>
    <hr>
    <!--- The info section --->
    <div id="info-section">
        Scandiweb test assignment<br>
        Completed by Verners Mednis (vmxxxxx@inbox.lv)
    </div>
</body>
</html>
