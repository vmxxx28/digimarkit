<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Product extends Eloquent
{
    protected $fillable = ['SKU', 'name', 'price', 'product_specific_attribute'];
}